# Description: Simple script to replace all creatures images inside a compandium beastiary with matching filenames from a target folder
# By: Nord for FoundryrVTT - PF2 community
# requirement: pip install ndjson

# Howto:
# step 1: copy your image folder to /Foundry/Data/worlds/icons/. This script does not support subfolders yet.
# step 2: install python
# step 3  run: imagemapper.py -db <path to compendium .db file> -i <path to creature image folder>
#         example: python imagemapper.py -db /Users/myuser/Foundry/Data/worlds/my-hello-world/packs/pathfinder-bestiary.db -i /Users/myuser/Foundry/Data/worlds/my-hello-world/icons/bestiary
# step 4: look at the output and see what went wrong. You may have to rename images, the closer to creature name in compandium the better.
#         you can restart this script as many times as you want
# step 5: restart foundry it was running already
# step 6: enjoy your gaming session

import ndjson # .db file is stored as a newline delimited JSON which is why we use the special library ndjson instead of regular json
import os
import argparse
import re
import sys


def nicername(text):
    text = text.lower()
    text = re.sub(r" ?\([^)]+\)", "", text) # ignore all text inside paranthesis, it's usually only creature type anyway
    text = re.sub("\d+", "", text) # remove numbers from name
    for ch in [' ','_','-','[',']','’']: # remove weird characters and dragon age
        if ch in text:
            text = text.replace(ch,"")
    return text


def shortimage(text):
    return text.split('/')[-1].split('.')[0] # onliner to convert full path filename into filename without extension.


def worldpath(text):
    text = text.replace("\\","/").split("/")[-5:] # convert windows formating and pull out only the path inside foundry directory
    return '/'.join(text)


def validfilename(text):
    if(text.split('.')[-1] in ['jpg','jpeg','png','svg','gif']):
        return True
    else:
        return False


parser = argparse.ArgumentParser(description='input the full path to your compendium database and image folder using two required arguments.')
parser.add_argument('-i','--images', required=True, help='type in the full path to the folder where images is stored')
parser.add_argument('-d','-db','--database', required=True, help='type in the full path to the .db file you want to change')
args = parser.parse_args()

if not (os.path.exists(args.database)):
    sys.exit('wrong path to compendium .db file')

if not (os.path.isdir(args.images)):
    sys.exit('wrong path picture library directory')

if not "worlds" in args.images:
    sys.exit('you need to put your image folder in your foundry world')

if not "icons" in args.images:
    sys.exit('you need to put your image folder inside the icons map')

with open(args.database, encoding='utf-8') as json_file:
    compendium = ndjson.load(json_file)

images = [os.path.join(r,file) for r,d,f in os.walk(args.images) for file in f]
i = 0

for beast in compendium:
    match = ""
    for image in images:
        if not (validfilename(image)):
            continue
        if(nicername(shortimage(image)) == nicername(beast['name'])): # no need to search anymore, we found exact match
            match = image
            break
        if(len(nicername(beast['name'])) > 3 and nicername(beast['name']) in nicername(shortimage(image))): # we are confident this is the right one since it matches exact name in file name
            match = image
            break
        if(len(nicername(beast['name'])) < 4 and nicername(beast['name']) in nicername(shortimage(image))): # we are not so confident
            match = image
        if(nicername(shortimage(image)) in nicername(beast['name'])): # as a last resort we can try reverse order
            match = image

    if(len(match)):
        i += 1
        print("added: " + match + " to: " +beast['name'])
        beast['img'] = worldpath(match)
        beast['token']['img'] = worldpath(match)
    else:
        print("did not find a match for " + beast['name'])
    
print("found " + str(i) + " matches to " + str(len(compendium)) + " creatures")

with open(args.database, 'w') as f:
    ndjson.dump(compendium, f)
imagemapper

Description: Simple script to replace all creatures images inside a compandium beastiary with matching filenames from a target folder

By: Nord for FoundryrVTT - PF2 community

requirement: ``pip install ndjson``
also, make sure you have copied bestiary (or the compandium file you to change) to your world.

**Howto**:

step 1:

copy your image folder to /Foundry/Data/worlds/icons/. This script does not support subfolders yet.

step 2:

install python 

step 3: 

run ``python imagemapper.py -db <path to compendium .db file> -i <path to creature image folder>``

example ``python imagemapper.py -db /Users/myuser/Foundry/Data/worlds/my-hello-world/packs/pathfinder-bestiary.db -i /Users/myuser/Foundry/Data/worlds/my-hello-world/icons/bestiary``

step 4:

look at the output and see what went wrong. You may have to rename images, the closer to creature name in compandium the better.

you can restart this script as many times as you want

step 5:
restart foundry it was running 

step 6:

enjoy your gaming session
